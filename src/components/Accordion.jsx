import React from "react"

export default function Accordion({ active = false, items = [], onClick, title = "Give me the title" }) {
  return (
    <div className="accordion" onClick={onClick}>
      <div className={`accordion-header ${active ? "active" : ""}`}>
        <p>{title}</p>
        <i className={`bi bi-chevron-down ${active ? "active" : ""}`} />
      </div>
      <div className={`accordion-items ${active ? "active" : ""}`}>
        {items.map((i) => (
          <div className={`accordion-item ${active ? "active" : ""}`} key={i}>
            {i}
          </div>
        ))}
      </div>
    </div>
  )
}
