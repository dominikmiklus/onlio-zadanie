import React, { useState } from "react"
import Accordion from "./components/Accordion"

export default function App() {
  const [activeAccordions, setActiveAccordions] = useState([false, false])

  const items1 = [
    "Phasellus quis blandit mauris, eget pharetra magna. Duis quis nisl eget ante tincidunt sagittis vitae at lacus. Integer at diam dolor. Suspendisse eleifend mattis lectus eget malesuada. Quisque nec mauris placerat, sagittis nisl ac, tempus enim. Nulla finibus varius aliquet.",
    "Quisque in dui turpis. Suspendisse risus est, mattis id odio sit amet, ornare tempor orci. Etiam mattis pharetra orci, a tincidunt ipsum condimentum vel.",
  ]

  const items2 = [
    "Quisque in dui turpis. Suspendisse risus est, mattis id odio sit amet, ornare tempor orci. Etiam mattis pharetra orci, a tincidunt ipsum condimentum vel.",
  ]

  const handleAccordionClick = (accordionKey) => {
    // change the state of 0 accordion
    if (accordionKey === 0) {
      setActiveAccordions([!activeAccordions[0], activeAccordions[1]])
    }
    // the only case both states are changed
    else if (accordionKey === 1 && activeAccordions[0] && !activeAccordions[1]) {
      setActiveAccordions([!activeAccordions[0], !activeAccordions[1]])
    }
    // change the state of 1 accordion
    else {
      setActiveAccordions([activeAccordions[0], !activeAccordions[1]])
    }
  }

  return (
    <div className="container">
      <Accordion items={items1} onClick={() => handleAccordionClick(0)} active={activeAccordions[0]} />
      <Accordion items={items2} onClick={() => handleAccordionClick(1)} active={activeAccordions[1]} />
    </div>
  )
}
